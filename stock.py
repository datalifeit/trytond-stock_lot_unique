# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from sql import Column
from trytond.model import ModelView, fields, Unique
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    @classmethod
    def __setup__(cls):
        super(Lot, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('lot_uniq', Unique(t, t.number, t.product),
                'stock_lot_unique.msg_lot_uniq_by_product'),
        ]


class LotReplace(Wizard):
    """Replace Lot"""
    __name__ = 'stock.lot.replace'

    start_state = 'ask'
    ask = StateView('stock.lot.replace.ask',
        'stock_lot_unique.replace_ask_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Replace', 'replace', 'tryton-find-replace', default=True),
            ])
    replace = StateTransition()

    def check_similarity(self):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        sources = self.ask.sources
        destination = self.ask.destination
        for source in sources:
            if source.number != destination.number:
                key = 'stock.lot.replace number %s %s' % (
                    source.number, destination.number)
                if Warning.check(key):
                    raise UserWarning(key, gettext(
                        'stock_lot_unique.msg_lot_different_name',
                        source_name=source.number,
                        destination_name=destination.number))
            if source.product.id != destination.product.id:
                raise UserError(gettext(
                    'stock_lot_unique.msg_lot_different_product',
                    source_code=source.product.rec_name,
                    destination_code=destination.product.rec_name))

    def check_period_cache(self):
        pool = Pool()
        Cache = pool.get('stock.period.cache.lot')

        lot_ids = [s.id for s in self.ask.sources] + [self.ask.destination.id]
        caches = Cache.search([('lot', 'in', lot_ids)], limit=1)
        if caches:
            raise UserError(gettext(
                'stock_lot_unique.msg_lot_period_cache',
                period=caches[0].period.rec_name))

    def transition_replace(self):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lot = Lot.__table__()
        transaction = Transaction()

        self.check_period_cache()
        self.check_similarity()
        source_ids = [s.id for s in self.ask.sources]
        destination = self.ask.destination

        cursor = transaction.connection.cursor()
        for model_name, field_name in self.fields_to_replace():
            Model = pool.get(model_name)
            table = Model.__table__()
            column = Column(table, field_name)
            where = column.in_(source_ids)

            if transaction.database.has_returning():
                returning = [table.id]
            else:
                cursor.execute(*table.select(table.id, where=where))
                ids = [x[0] for x in cursor]
                returning = None

            cursor.execute(*table.update(
                    [column],
                    [destination.id],
                    where=where,
                    returning=returning))

            if transaction.database.has_returning():
                ids = [x[0] for x in cursor]

            Model._insert_history(ids)

        # delete lots
        cursor.execute(*lot.delete(
            where=lot.id.in_(source_ids)))

        return 'end'

    @classmethod
    def fields_to_replace(cls):
        return [
            ('stock.move', 'lot'),
            ('stock.inventory.line', 'lot'),
            ]

    def end(self):
        return 'reload'


class LotReplaceAsk(ModelView):
    """Replace Lot Ask"""
    __name__ = 'stock.lot.replace.ask'

    sources = fields.One2Many('stock.lot', None, 'Source', required=True,
        help='The lots to be replaced.')
    destination = fields.Many2One('stock.lot', 'Destination', required=True,
        domain=[
            ('id', 'not in', Eval('sources', -1)),
            ],
        depends=['sources'],
        help='The lot that replaces.')

    @classmethod
    def default_sources(cls):
        context = Transaction().context
        if context.get('active_model') == 'stock.lot':
            ids = context.get('active_ids')
            if ids:
                # remove first of them
                ids = ids[1:]
            return ids

    @classmethod
    def default_destination(cls):
        context = Transaction().context
        if context.get('active_model') == 'stock.lot':
            return context.get('active_id')
