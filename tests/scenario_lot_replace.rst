======================
Lot Replace Scenario
======================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()

Install Stock lot unique::

    >>> config = activate_modules('stock_lot_unique')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create a lot::

    >>> Lot = Model.get('stock.lot')
    >>> lot1 = Lot(number='L1', product=product)
    >>> lot1.save()

Create a second lot::

    >>> lot2 = Lot(number='L-1', product=product)
    >>> lot2.save()

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Add two shipment lines of same product::

    >>> StockMove = Model.get('stock.move')
    >>> move = StockMove()
    >>> shipment_out.outgoing_moves.append(move)
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 10
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> move = StockMove()
    >>> shipment_out.outgoing_moves.append(move)
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 4
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_out.save()

Set the shipment state to waiting::

    >>> shipment_out.click('wait')
    >>> len(shipment_out.outgoing_moves)
    2
    >>> len(shipment_out.inventory_moves)
    2

Assign the shipment with 2 lines of 7 products::

    >>> for move in shipment_out.inventory_moves:
    ...     move.quantity = 7
    >>> shipment_out.click('assign_force')
    >>> shipment_out.state
    'assigned'

Set 2 lots::

    >>> Lot = Model.get('stock.lot')
    >>> shipment_out.inventory_moves[0] = lot1
    >>> shipment_out.inventory_moves[1] = lot2
    >>> shipment_out.save()

Replace the second by the first lot::

    >>> replace = Wizard('stock.lot.replace', models=[lot1,lot2])
    >>> replace.form.sources == [lot2]
    True
    >>> replace.form.destination == lot1
    True
    >>> replace.execute('replace')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: Lots have different numbers: L-1 vs L1. - 

    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='stock.lot.replace number L-1 L1', always=True).save()
    >>> replace.execute('replace')

    >>> lot1, = Lot.find([])
    >>> not StockMove.find([('lot', '=', lot2.id)])
    True