# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool
from trytond.exceptions import UserError


class StockLotUniqueTestCase(ModuleTestCase):
    """Test Stock Lot Unique module"""
    module = 'stock_lot_unique'

    @with_transaction()
    def test_move_internal_quantity(self):
        'Test Move.internal_quantity'
        pool = Pool()
        Uom = pool.get('product.uom')
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')

        kg, = Uom.search([('name', '=', 'Kilogram')])
        g, = Uom.search([('name', '=', 'Gram')])
        template, = Template.create([{
            'name': 'Test Move.internal_quantity',
            'type': 'goods',
            'list_price': Decimal(1),
            'cost_price_method': 'fixed',
            'default_uom': kg.id,
        }])
        product, = Product.create([{
            'template': template.id,
        }])

        Lot.create([{
            'number': 'L-123',
            'product': product.id
            }, {
            'number': 'L-456',
            'product': product.id}
        ])
        self.assertRaises(UserError, Lot.create, [{
            'number': 'L-123',
            'product': product.id}])


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            StockLotUniqueTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_lot_replace.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
